
### Les triggers

Un trigger doit être déclenché par un événement.
Ainsi soit un CREATE, DELETE ou encore un UPDATE.
Le trigger est déclenché pour chaque enregistrement impacté.

Ca peut être utilisé pour historiser des actions par exemple, ou encore déclencher des mises à jour de données. 

```sql
DELIMITER |
CREATE TRIGGER before_update_animal BEFORE UPDATE
ON Animal FOR EACH ROW
BEGIN
    IF NEW.sexe IS NOT NULL   -- le sexe n'est ni NULL
    AND NEW.sexe != 'M'       -- ni "M"âle
    AND NEW.sexe != 'F'       -- ni "F"emelle
      THEN
        SET NEW.sexe = NULL;
    END IF;
END |
DELIMITER ;
```


Plusieurs choses sont à préciser : 
 - Le `FOR EACH ROW` est obligatoire
 - `CREATE OR REPLACE` n'est pas interpreté par MySQL. Il est nécessaire de passer par un `DROP` puis `CREATE`.
 - Si votre base de données risque d'être sollicitée lors de vos opérations. Pensez à réaliser un `LOCK` de la table.
 - Les mots clés pour accéder à l'ancien et nouvel enregistrement sont respectivement `OLD` et `NEW`.


#### Exercice

Réaliser un trigger qui stocke au sein d'une nouvelle table les changements de tarification d'un groupe.